'use strict';

module.exports = {
  db: 'mongodb://localhost/mean-dev-earth',
  app: {
    name: 'Welcome to Earth Eyes'
  },
  facebook: {
    clientID: '757886177592708',
    clientSecret: 'f6bdd85d1707d73294e47a4d77832e3c',
    callbackURL: 'http://www.earth-eyes.net:3000/auth/facebook/callback'
  },
  twitter: {
    clientID: 'RNJa5np4MOipqGMjQHLvhN1Hi',
    clientSecret: 'cXYE4OW4NLVyfW68r85wO864JDIx9UDGNinz4z2dEkbbGuKukp',
    callbackURL: 'http://localhost:3000/auth/twitter/callback'
  },
  github: {
    clientID: 'APP_ID',
    clientSecret: 'APP_SECRET',
    callbackURL: 'http://localhost:3000/auth/github/callback'
  },
  google: {
    clientID: 'APP_ID',
    clientSecret: 'APP_SECRET',
    callbackURL: 'http://localhost:3000/auth/google/callback'
  },
  linkedin: {
    clientID: 'API_KEY',
    clientSecret: 'SECRET_KEY',
    callbackURL: 'http://localhost:3000/auth/linkedin/callback'
  },
  emailFrom: 'SENDER EMAIL ADDRESS', // sender address like ABC <abc@example.com>
  mailer: {
    service: 'SERVICE_PROVIDER',
    auth: {
      user: 'EMAIL_ID',
      pass: 'PASSWORD'
    }
  }
};
