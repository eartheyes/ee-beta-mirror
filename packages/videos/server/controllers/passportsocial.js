'use strict';

var request = require('request'),
	nodemailer = require('nodemailer'),
  config = require('meanio').loadConfig();


exports.socialShare = function(video,req){
	var postinfo = {
		'message': video.title +' '+ req.protocol + '://' + req.get('host')+'/#!/player/'+video._id,
		'link': req.protocol + '://' + req.get('host')+'/#!/player/'+video._id
	};
	this.confirmMail(video,req);
	if(req.user.provider === 'facebook'){
		return this.fbPublish(postinfo,req.user);
	}
	if(req.user.provider === 'twitter'){
		return this.twitterPublish(postinfo,req.user);
	}
};

exports.confirmMail = function(video,req){

	var transporter = nodemailer.createTransport();
	transporter.sendMail({
    	from: 'algo60@gmail.com',
	    to: req.user.email,
	    subject: video.title+ 'uploaded',
	    text: 'new video is uploaded'
	}, function(err, res) {
		if (err) {
			console.log(err);
		}
		console.log(res);
	});
	
};

exports.fbPublish = function(postinfo,user){
	request.get({url: 'https://graph.facebook.com//oauth/access_token', qs: {
		client_id: config.facebook.clientID,
      	client_secret: config.facebook.clientSecret,
      	grant_type: 'fb_exchange_token',
      	fb_exchange_token:user.facebook.accessToken
	}}, function(err, fbres)	 {
		var data = fbres.body.split('&');
		var params = {};
		for(var i=0; i<data.length; i++) {
		    var item = data[i].split('=');
		    params[item[0]] = item[1];
		}
		request.post({url: 'https://graph.facebook.com/me/feed', qs: {
	        access_token: params.access_token,
	        message: postinfo.message,
	        link: postinfo.link

	    }}, function(err, resp, body) {
	    	return true;
	    });
	});
};

exports.twitterPublish = function(postinfo,user){
	request.post({url: 'https://api.twitter.com/1.1/statuses/update.json', qs: {
		status: postinfo.message,
	}}, function(err, resp)	 {
		return true;
	});
};