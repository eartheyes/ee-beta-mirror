'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Course = new Module('course');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Course.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Course.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  Course.menus.add({
    title: 'My Courses',
    link: 'course.student',
    roles: ['Student'],
    menu: 'main'
  });

  Course.menus.add({
    title: 'Manage Courses',
    link: 'course.manage',
    roles: ['Instructor'],
    menu: 'main'
  });

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Course.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Course.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Course.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Course;
});
