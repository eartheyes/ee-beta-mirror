'use strict';

angular.module('mean.course').controller('CourseController', ['$scope', 'Global', 'Course',
  function($scope, Global, Course) {
    $scope.global = Global;
    $scope.package = {
      name: 'course'
    };
  }
]);

angular.module('mean.course').controller('CourseStudentController', ['$scope','$sce','$stateParams',
    function($scope,$sce,$stateParams) {
        
        $scope.testvideos = [
        	{
        		'link':'https://www.youtube.com/embed/08dhTL1DgFU',
        		'name':'Siding Spring live shot with Jared Espley',
        	},
        	{
        		'link':'https://www.youtube.com/embed/9V3EBqroqYY',
        		'name':' Siding Spring live shot with Micheal',
        	},
        	{
        		'link':'https://www.youtube.com/embed/FG4KsatjFeI',
        		'name':'Observing Comet Siding Spring at Mars',
        	},
        	{
        		'link':'https://www.youtube.com/embed/s9BmvcI4ops',
        		'name':' OIB: Four-wheeling Antarctica',
        	}
        ];

        $scope.extvideo = null;

        $scope.playVideo = function(video){
        	$scope.extvideo = video.link;
        };

        $scope.trustSrc = function(src) {
			return $sce.trustAsResourceUrl(src);
		};
    }
]);

