'use strict';

angular.module('mean.course').config(['$stateProvider',
  function($stateProvider) {
    
    // states for my app
        $stateProvider
        .state('course', {
        	url: '/course',
            templateUrl: 'course/views/index.html'
        })
        .state('course.student', {
			url : '/student',
			views: {
				'student':{
					templateUrl : 'course/views/student.html'
				}
			}	
		})
		.state('course.manage', {
			url : '/manage',
			views: {
				'instructor':{
					templateUrl : 'course/views/manage.html'
				}
			}
		});
		
            
  }
]);
